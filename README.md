# vmhack

[![pipeline status](https://gitlab.com/me-learnz/nand2tetris/vmhack/badges/master/pipeline.svg)](https://gitlab.com/me-learnz/nand2tetris/vmhack/pipelines)

A VM-to-assembly translator for a Hack computer developed in a course [From NAND to Tetris: Building a Modern Computer From First Principles](http://www.nand2tetris.org).