use std::io::{Write, BufWriter};
use std::fs::File;
use std::io::Result;
use std::path::PathBuf;

use cmd_types::{CmdAsm, CCmd, CmdVM, CmdFunc, ArithVar, MemSegment, FuncVar};

pub struct CodeWriter {
    pub vm_file: Option<PathBuf>,
    writer: BufWriter<File>,
    jmp_cnt: (u16, u16, u16),     // counting (eq, gt, lt) commands
    call_cnt: u16,                // counting function calls
    current_func: Option<String>, // current function scope
}

impl CodeWriter {
    /// Open a file in `path` and return a new `CodeWriter`.
    pub fn new(path: &PathBuf) -> Result<CodeWriter> {
        let fh = File::create(path)?;
        let code_writer = CodeWriter {
            vm_file: None,
            writer: BufWriter::new(fh),
            jmp_cnt: (0, 0, 0),
            call_cnt: 0,
            current_func: None,
        };
        Ok(code_writer)
    }

    /// Change current VM file and write the path to the output file as comment
    pub fn set_file(&mut self, path: &PathBuf) {
        let stem = path.file_name().expect("Error extracting file stem from \
                                           `path`");
        let stem = stem.to_string_lossy().into_owned();
        self.writer.write_fmt(format_args!("// file {}\n", stem))
            .expect("Error writing filename to output file");
        self.vm_file = Some(PathBuf::from(path));
    }

    /// Write a command (covers all types of commands)
    pub fn write_cmd(&mut self, cmd: &mut CmdVM) {
        // Update current function scope, if necessary
        self.update_func(cmd);

        // Generate unique identifier
        let uid = self.uid(cmd);

        // Print command names in comments when compiled in a debug mode.
        // Nothing written in a release mode.
        #[cfg(debug_assertions)]
        self.writer.write_all(&format!("// {:?}\n", cmd).as_bytes())
            .expect("Error writing debug to output file");

        // Write the command translated into a series of ASM commands
        self.write_cmd_vec(&cmd.translate(&uid));
    }

    /// Write initial bootstrap code (SP = 256; call Sys.init; endless loop)
    pub fn write_init(&mut self) {
        let sp_init = vec![
            CmdAsm::A("256"),
            CmdAsm::C(CCmd{ dest: Some("D") , comp: "A", jump: None }),
            CmdAsm::A("SP"),
            CmdAsm::C(CCmd{ dest: Some("M") , comp: "D", jump: None }),
        ];
        self.write_cmd_vec(&sp_init);

        let call_sys_init = CmdFunc::from("call", "Sys.init", "0")
            .expect("Error creating Sys.init call");
        let mut call_sys_init = CmdVM::Func(call_sys_init);
        self.write_cmd(&mut call_sys_init);

        let halt_loop = vec![
            CmdAsm::L("HALT_LOOP"),
            CmdAsm::A("HALT_LOOP"),
            CmdAsm::C(CCmd{ dest: None, comp: "0", jump: Some("JMP") }),
        ];
        self.write_cmd_vec(&halt_loop);
    }

    /// Write vector of assembly commands
    fn write_cmd_vec(&mut self, cmd_vec: &Vec<CmdAsm>) {
        for asm_cmd in cmd_vec {
            let mut transl = match asm_cmd {
                &CmdAsm::L(_) => String::new(),
                _ => String::from("    "),
            };
            transl.push_str(&asm_cmd.to_string());
            transl.push('\n');
            self.writer.write_all(transl.as_bytes())
                .expect("Error writing command to output file");
        }
    }

    /// Generate unique identifier, depending on the parsed command
    fn uid(&mut self, cmd: &CmdVM) -> String {
        let mut uid = String::new();
        match cmd {
            &CmdVM::Arith(ref cmd_arith) => {
                match cmd_arith.cmd {
                    ArithVar::Eq => {
                        uid = self.jmp_cnt.0.to_string();
                        self.jmp_cnt.0 += 1;
                    },
                    ArithVar::Gt => {
                        uid = self.jmp_cnt.1.to_string();
                        self.jmp_cnt.1 += 1;
                    },
                    ArithVar::Lt => {
                        uid = self.jmp_cnt.2.to_string();
                        self.jmp_cnt.2 += 1;
                    },
                    _ => (),
                }

            },
            &CmdVM::Mem(ref cmd_mem) => {
                if let MemSegment::Static = cmd_mem.segment {
                    let stem = match self.vm_file {
                        Some(ref file) => {
                            file.file_stem().expect("Error extracting file \
                                                    stem from file")
                        },
                        None => panic!("CodeWriter has no `vm_file` set. \
                                       Consider calling `set_file()`."),
                    };
                    uid = stem.to_string_lossy().into_owned();
                }
            },
            &CmdVM::Flow(_) => {
                uid = self.current_func.clone().unwrap_or(String::new());
            },
            &CmdVM::Func(ref cmd_func) => {
                if let FuncVar::Call = cmd_func.cmd {
                    uid = self.call_cnt.to_string();
                    self.call_cnt += 1;
                }
            },
        }

        uid
    }

    /// Update current function scope (used for flow command labels)
    fn update_func(&mut self, cmd: &CmdVM) {
        if let &CmdVM::Func(ref cmd_func) = cmd {
            if let FuncVar::Function = cmd_func.cmd {
                self.current_func = Some(cmd_func.func.clone());
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use tempdir::TempDir;
    use cmd_types::{CmdArith, CmdMem, CmdFunc, CmdVM};
    use super::CodeWriter;

    #[test]
    fn update_func() {
        let dir = TempDir::new("vmhack_test").unwrap();
        let file_path = dir.path().join("test.asm");
        let mut writer = CodeWriter::new(&file_path).unwrap();

        // mock VM file
        let mut vm_file = PathBuf::new();
        vm_file.push("test.vm");
        writer.set_file(&vm_file);

        // Non-function command
        let cmd = CmdVM::Mem(CmdMem::from("push", "constant", "5").unwrap());
        CodeWriter::update_func(&mut writer, &cmd);
        assert_eq!(writer.current_func, None);
        // Declare Sys.init
        let cmd = CmdVM::Func(CmdFunc::from("function", "Sys.init", "0")
            .unwrap());
        CodeWriter::update_func(&mut writer, &cmd);
        assert_eq!(writer.current_func, Some(String::from("Sys.init")));
        // Declare function (allowed inside Sys.init)
        let cmd = CmdVM::Func(CmdFunc::from("function", "bagr", "0").unwrap());
        CodeWriter::update_func(&mut writer, &cmd);
        assert_eq!(writer.current_func, Some(String::from("bagr")));
    }

    #[test]
    fn uid_static() {
        let dir = TempDir::new("vmhack_test").unwrap();
        let file_path = dir.path().join("test.asm");
        let mut writer = CodeWriter::new(&file_path).unwrap();

        // VM file 1
        let mut vm_file = PathBuf::new();
        vm_file.push("test.vm");
        writer.set_file(&vm_file);
        let cmd = CmdVM::Mem(CmdMem::from("push", "static", "3").unwrap());
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), String::from("test"));

        // VM file 2
        let mut vm_file = PathBuf::new();
        vm_file.push("foo.vm");
        writer.set_file(&vm_file);
        let cmd = CmdVM::Mem(CmdMem::from("pop", "static", "5").unwrap());
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), String::from("foo"));
    }

    #[test]
    fn uid_jmp() {
        let dir = TempDir::new("vmhack_test").unwrap();
        let file_path = dir.path().join("test.asm");
        let mut writer = CodeWriter::new(&file_path).unwrap();

        let cmd = CmdVM::Arith(CmdArith::from("eq").unwrap());
        let expected = String::from("0");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);
        let expected = String::from("1");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);
        let expected = String::from("2");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);

        let cmd = CmdVM::Arith(CmdArith::from("gt").unwrap());
        let expected = String::from("0");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);
        let expected = String::from("1");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);

        let cmd = CmdVM::Arith(CmdArith::from("lt").unwrap());
        let expected = String::from("0");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);

        let cmd = CmdVM::Arith(CmdArith::from("eq").unwrap());
        let expected = String::from("3");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);

        let cmd = CmdVM::Arith(CmdArith::from("gt").unwrap());
        let expected = String::from("2");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);

        let cmd = CmdVM::Arith(CmdArith::from("lt").unwrap());
        let expected = String::from("1");
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), expected);
    }

    #[test]
    fn uid_call() {
        let dir = TempDir::new("vmhack_test").unwrap();
        let file_path = dir.path().join("test.asm");
        let mut writer = CodeWriter::new(&file_path).unwrap();

        let cmd = CmdVM::Func(CmdFunc::from("call", "foo", "0").unwrap());
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), String::from("0"));
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), String::from("1"));
        assert_eq!(CodeWriter::uid(&mut writer, &cmd), String::from("2"));
    }
}
