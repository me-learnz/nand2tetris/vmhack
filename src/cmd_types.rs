use std::fmt;

use self::CmdAsm::{A, C, L};

#[derive(Debug)]
pub enum CmdVM {
    Arith(CmdArith),
    Mem(CmdMem),
    Flow(CmdFlow),
    Func(CmdFunc),
}

impl CmdVM {
    pub fn translate(&mut self, uid: &str) -> Vec<CmdAsm> {
        match *self {
            CmdVM::Arith(ref mut cmd) => cmd.translate(uid),
            CmdVM::Mem(ref mut cmd) => cmd.translate(uid),
            CmdVM::Flow(ref mut cmd) => cmd.translate(uid),
            CmdVM::Func(ref mut cmd) => cmd.translate(uid),
        }
    }
}

struct TranslLib {
    incr_sp: [CmdAsm<'static>; 2],
    from_stack: [CmdAsm<'static>; 3],
    d_to_stack: [CmdAsm<'static>; 3],
    d_to_r13: [CmdAsm<'static>; 2],
    d_to_r14: [CmdAsm<'static>; 2],
    d_to_r13_addr: [CmdAsm<'static>; 3],
    push_d_to_stack: [CmdAsm<'static>; 4],
    pop_from_stack_to_d: [CmdAsm<'static>; 3],
    pop_from_r13_to_d: [CmdAsm<'static>; 3],
}

static LIB: TranslLib = TranslLib {
    // Mem[SP] = Mem[SP+1]
    incr_sp: [
        A("SP"),
        C(CCmd{ dest: Some("M"), comp: "M+1", jump: None }),
    ],
    // Access Mem[Mem[SP-1]]
    from_stack: [
        A("SP"),
        C(CCmd{ dest: Some("M"), comp: "M-1", jump: None }),
        C(CCmd{ dest: Some("A"), comp: "M",   jump: None }),
    ],
    // Store D to Mem[Mem[SP]]
    d_to_stack: [
        A("SP"),
        C(CCmd{ dest: Some("A"), comp: "M",   jump: None }),
        C(CCmd{ dest: Some("M"), comp: "D",   jump: None }),
    ],
    // Save D to R13
    d_to_r13: [
        A("R13"),
        C(CCmd{ dest: Some("M"), comp: "D",   jump: None }),
    ],
    // Save D to R14
    d_to_r14: [
        A("R14"),
        C(CCmd{ dest: Some("M"), comp: "D",   jump: None }),
    ],
    // Save D into addr in R13
    d_to_r13_addr: [
        A("R13"),
        C(CCmd{ dest: Some("A"), comp: "M",   jump: None }),
        C(CCmd{ dest: Some("M"), comp: "D",   jump: None }),
    ],
    // Push D to stack and increment SP (more efficient approach)
    push_d_to_stack: [
        A("SP"),
        C(CCmd{ dest: Some("AM"), comp: "M+1",   jump: None }),
        C(CCmd{ dest: Some("A"), comp: "A-1",   jump: None }),
        C(CCmd{ dest: Some("M"), comp: "D",   jump: None }),
    ],
    // Pop from stack to D register
    pop_from_stack_to_d: [
        A("SP"),
        C(CCmd{ dest: Some("AM"), comp: "M-1", jump: None }),
        C(CCmd{ dest: Some("D"), comp: "M",   jump: None }),
    ],
    // Pop from R13 to D register
    pop_from_r13_to_d: [
        A("R13"),
        C(CCmd{ dest: Some("AM"), comp: "M-1", jump: None }),
        C(CCmd{ dest: Some("D"), comp: "M",   jump: None }),
    ],
};

/// Arithmetic command
#[derive(Debug)]
pub struct CmdArith {
    pub cmd: ArithVar,
    jmp_end: String,  // Just a helper field to provide stable borrows
}

/// Possible variants of an arithmetic command
#[derive(Debug)]
pub enum ArithVar {
    Add,
    Sub,
    Neg,  // unary
    Eq,
    Gt,
    Lt,
    And,
    Or,
    Not,  // unary
}

impl CmdArith {
    /// Create a new instance of CmdArith
    pub fn from(cmd: &str) -> Result<CmdArith, String> {
        let jmp_end = String::new();
        match cmd {
            "add" => Ok(CmdArith { cmd: ArithVar::Add, jmp_end }),
            "sub" => Ok(CmdArith { cmd: ArithVar::Sub, jmp_end }),
            "neg" => Ok(CmdArith { cmd: ArithVar::Neg, jmp_end }),
            "eq"  => Ok(CmdArith { cmd: ArithVar::Eq, jmp_end }),
            "gt"  => Ok(CmdArith { cmd: ArithVar::Gt, jmp_end }),
            "lt"  => Ok(CmdArith { cmd: ArithVar::Lt, jmp_end }),
            "and" => Ok(CmdArith { cmd: ArithVar::And, jmp_end }),
            "or"  => Ok(CmdArith { cmd: ArithVar::Or, jmp_end }),
            "not" => Ok(CmdArith { cmd: ArithVar::Not, jmp_end }),
            _ => Err(format!("Invald arithmetic command `{}`.", cmd)),
        }
    }

    /// Translate itself into a series of assembly commands
    pub fn translate(&mut self, uid: &str) -> Vec<CmdAsm> {
        use self::ArithVar::*;

        let op = match self.cmd {
            Add => "D+M",
            Sub => "M-D",
            Neg => "-M",
            Eq  => "M-D",
            Gt  => "M-D",
            Lt  => "M-D",
            And => "D&M",
            Or  => "D|M",
            Not => "!M",
        };

        let (jmp, jmp_end) = match self.cmd {
            Eq => ("JEQ", "EQ_END"),
            Gt => ("JGT", "GT_END"),
            Lt => ("JLT", "LT_END"),
            _ => ("", ""),
        };

        self.jmp_end = [jmp_end, uid].join(".");

        // First read from stack is common for all commands
        let mut cmd_seq: Vec<CmdAsm> = Vec::new();
        cmd_seq.extend_from_slice(&LIB.from_stack);
        // Assemble command sequence
        match self.cmd {
            Add | Sub | And | Or => {
                cmd_seq.push(C(CCmd{ dest: Some("D"), comp: "M",  jump: None }));
                cmd_seq.extend_from_slice(&LIB.from_stack);
                cmd_seq.push(C(CCmd{ dest: Some("M"), comp: op,  jump: None }));
            },
            Neg | Not => {
                cmd_seq.push(C(CCmd{ dest: Some("M"), comp: op,  jump: None }));
            },
            Eq | Gt | Lt => {
                cmd_seq.push(C(CCmd{ dest: Some("D"), comp: "M",  jump: None }));
                cmd_seq.extend_from_slice(&LIB.from_stack);
                cmd_seq.push(C(CCmd{ dest: Some("D"), comp: op,  jump: None }));
                cmd_seq.extend(vec![
                    A("SP"),
                    C(CCmd{ dest: Some("A"), comp: "M", jump: None }),
                    C(CCmd{ dest: Some("M"), comp: "-1", jump: None }),
                    A(&self.jmp_end),
                    C(CCmd{ dest: None, comp: "D", jump: Some(jmp) }),
                    A("SP"),
                    C(CCmd{ dest: Some("A"), comp: "M", jump: None }),
                    C(CCmd{ dest: Some("M"), comp: "0", jump: None }),
                    L(&self.jmp_end),
                ]);
            }
        }
        // Incrementing SP is common for all commands
        cmd_seq.extend_from_slice(&LIB.incr_sp);
        cmd_seq
    }
}

/// Memory access command
#[derive(Debug)]
pub struct CmdMem {
    pub cmd: MemVar,
    pub segment: MemSegment,
    index: String,
    static_uid: String // Storing uid for static commands
}

// Possible variants of a memory command
#[derive(Debug)]
pub enum MemVar {
    Push,
    Pop,
}

/// Possible memory segments
#[derive(Debug)]
pub enum MemSegment {
    Argument,
    Local,
    Static,
    Constant,
    This,
    That,
    Pointer,
    Temp,
}

impl CmdMem {
    /// Create a new instance of CmdMem
    pub fn from(cmd: &str,
                segment: &str,
                index: &str) -> Result<CmdMem, String>
    {
        let static_uid = String::new();

        let cmd = match cmd {
            "push" => MemVar::Push,
            "pop" => MemVar::Pop,
            _ => return Err(format!("`{}` is not `push` or `pop`.", cmd)),
        };

        let segment = match segment {
            "argument" => MemSegment::Argument,
            "local"    => MemSegment::Local,
            "static"   => MemSegment::Static,
            "constant" => MemSegment::Constant,
            "this"     => MemSegment::This,
            "that"     => MemSegment::That,
            "pointer"  => MemSegment::Pointer,
            "temp"     => MemSegment::Temp,
            _ => return Err(format!("Invalid memory segment `{}`.", segment)),
        };

        let index = match index.parse::<u16>() {
            Ok(idx) => {
                match segment {
                    MemSegment::Pointer => if idx > 1 {
                        return Err(format!("Invalid index={} for `pointer` \
                                           segment. Maximum is 1.", idx));
                    },
                    MemSegment::Temp => if idx > 7 {
                        return Err(format!("Invalid index={} for `temp` \
                                           segment. Maximum is 7.", idx));
                    },
                    _ => (),
                }
                String::from(index)
            },
            Err(e) => return Err(
                format!("Error translating `{}`. It has to be an unsigned \
                         16-bit integer. Original error: `{}`",
                         index, e)),
        };

        // `pop constant` fix
        if let (&MemVar::Pop, &MemSegment::Constant) = (&cmd, &segment) {
            return Err(format!("`pop constant` is not allowed."));
        }

        Ok(CmdMem { cmd, segment, index, static_uid })
    }

    /// Translate itself into a series of assembly commands
    pub fn translate(&mut self, uid: &str) -> Vec<CmdAsm> {
        use self::MemVar::*;
        use self::MemSegment::*;

        let segm_name = match self.segment {
            Local => "LCL",
            Argument => "ARG",
            This => "THIS",
            That => "THAT",
            Pointer => "3",
            Temp => "5",
            _ => "",
        };

        let loc = match self.segment {
            Local | Argument | This | That => "M",
            Pointer | Temp => "A",
            _ => "",
        };

        let jump = None;

        let mut cmd_seq: Vec<CmdAsm> = Vec::new();

        match self.cmd {
            Push => {
                match self.segment {
                    Local | Argument | This | That | Pointer | Temp => {
                        cmd_seq.extend(vec![
                            A(segm_name),
                            C(CCmd{ dest: Some("D"), comp: loc, jump }),
                            A(&self.index),
                            C(CCmd{ dest: Some("A"), comp: "D+A", jump }),
                            C(CCmd{ dest: Some("D"), comp: "M", jump }),
                        ]);
                    },
                    Constant => {
                        cmd_seq.extend(vec![
                            A(&self.index),
                            C(CCmd{ dest: Some("D"), comp: "A", jump }),
                        ]);
                    },
                    Static => {
                        self.static_uid.push_str(uid);
                        self.static_uid.push('.');
                        self.static_uid.push_str(&self.index);

                        cmd_seq.extend(vec![
                            A(&self.static_uid),
                            C(CCmd{ dest: Some("D"), comp: "M", jump }),
                        ]);
                    },
                }
                cmd_seq.extend_from_slice(&LIB.d_to_stack);
                cmd_seq.extend_from_slice(&LIB.incr_sp);
            },

            Pop => {
                match self.segment {
                    Local | Argument | This | That | Pointer | Temp => {
                        cmd_seq.extend(vec![
                            A(segm_name),
                            C(CCmd{ dest: Some("D"), comp: loc, jump }),
                            A(&self.index),
                            C(CCmd{ dest: Some("D"), comp: "D+A", jump }),
                        ]);
                    },
                    Constant => (),
                    Static => {
                        self.static_uid.push_str(uid);
                        self.static_uid.push('.');
                        self.static_uid.push_str(&self.index);

                        cmd_seq.extend(vec![
                            A(&self.static_uid),
                            C(CCmd{ dest: Some("D"), comp: "A", jump }),
                        ]);
                    },
                }
                cmd_seq.extend_from_slice(&LIB.d_to_r13);
                cmd_seq.extend_from_slice(&LIB.from_stack);
                cmd_seq.push(C(CCmd{ dest: Some("D"), comp: "M", jump }),);
                cmd_seq.extend_from_slice(&LIB.d_to_r13_addr);
            },
        }

        cmd_seq
    }
}

/// Program flow command
#[derive(Debug)]
pub struct CmdFlow {
    cmd: FlowVar,
    symbol: String,
}

#[derive(Debug)]
enum FlowVar {
    Label,
    Goto,
    IfGoto,
}

impl CmdFlow {
    /// Create a new instance of CmdFlow
    pub fn from(cmd: &str, symbol: &str) -> Result<CmdFlow, String>
    {
        let cmd = match cmd {
            "label"   => FlowVar::Label,
            "goto"    => FlowVar::Goto,
            "if-goto" => FlowVar::IfGoto,
            _ => return Err(format!("{} is not `label`, `goto` nor `if-goto`.",
                                    cmd)),
        };

        let symbol = check_symbol(symbol)?;

        Ok(CmdFlow { cmd, symbol })
    }

    /// Translate itself into a series of assembly commands
    pub fn translate(&mut self, uid: &str) -> Vec<CmdAsm> {
        use self::FlowVar::*;

        let mut cmd_seq: Vec<CmdAsm> = Vec::new();

        self.symbol = String::from(uid) + "$" + &self.symbol;

        match self.cmd {
            Label => {
                cmd_seq.push(L(&self.symbol));
            },
            Goto => {
                cmd_seq.extend(vec![
                    A(&self.symbol),
                    C(CCmd{ dest: None, comp: "0", jump: Some("JMP") }),
                ]);
            },
            IfGoto => {
                cmd_seq.extend_from_slice(&LIB.from_stack);
                cmd_seq.extend(vec![
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None }),
                    A(&self.symbol),
                    C(CCmd{ dest: None, comp: "D", jump: Some("JNE") }),
                ]);
            },
        }

        cmd_seq
    }
}

/// Function command
#[derive(Debug)]
pub struct CmdFunc {
    pub cmd: FuncVar,
    pub func: String,
    index: String,
    func_label: String,  // Label used in function declaration
    return_label: String // Label given to return address
}

#[derive(Debug)]
pub enum FuncVar {
    Call,
    Function,
    Return,
}

impl CmdFunc {
    /// Create a new instance of CmdFunc
    pub fn from(cmd: &str, func: &str, index: &str) -> Result<CmdFunc, String>
    {
        let cmd = match cmd {
            "call" => FuncVar::Call,
            "function" => FuncVar::Function,
            "return" => FuncVar::Return,
            _ => return Err(format!("{} is not `call`, `function` nor \
                                    `return`.", cmd)),
        };

        let mut func = String::from(func);
        let mut index = String::from(index);

        // Extra arguments when calling `return`
        if let FuncVar::Return = cmd {
            if !func.is_empty() || !index.is_empty() {
                return Err(format!("`return` command can't have any \
                                   arguments."));
            }
        } else {
            // Check for invalid function names
            func = check_symbol(&func)?;

            // Valid number check
            index = match index.parse::<u16>() {
                Ok(_) => String::from(index),
                Err(e) => return Err(
                    format!("Error translating `{}`. It has to be an unsigned \
                             16-bit integer. Original error: `{}`",
                             index, e)),
            };
        }

        let func_label = String::new();
        let return_label = String::new();

        Ok(CmdFunc { cmd, func, index, func_label, return_label })
    }

    /// Translate itself into a series of assembly commands
    pub fn translate(&mut self, uid: &str) -> Vec<CmdAsm> {
        use self::FuncVar::*;

        let mut cmd_seq: Vec<CmdAsm> = Vec::new();

        match self.cmd {
            Function => {
                self.func_label = self.func.clone() + "_push_local";

                cmd_seq.push(L(&self.func));

                if &self.index != "0" {
                    cmd_seq.extend(vec![
                        A(&self.index),
                        C(CCmd{ dest: Some("D"), comp: "A", jump: None }),
                        L(&self.func_label),
                        A("SP"),
                        C(CCmd{ dest: Some("AM"), comp: "M+1", jump: None }),
                        C(CCmd{ dest: Some("A"), comp: "A-1", jump: None }),
                        C(CCmd{ dest: Some("M"), comp: "0", jump: None }),
                        A(&self.func_label),
                        C(CCmd{ dest: Some("D"),
                                comp: "D-1",
                                jump: Some("JGT") }),
                    ]);
                }
            },
            Call => {
                self.return_label = ["ret_addr_call", uid].join(":");

                // push return address
                cmd_seq.extend(vec![
                    A(&self.return_label),
                    C(CCmd{ dest: Some("D"), comp: "A", jump: None })
                ]);
                cmd_seq.extend_from_slice(&LIB.push_d_to_stack);
                // push LCL
                cmd_seq.extend(vec![
                    A("LCL"),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None })
                ]);
                cmd_seq.extend_from_slice(&LIB.push_d_to_stack);
                // push ARG
                cmd_seq.extend(vec![
                    A("ARG"),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None })
                ]);
                cmd_seq.extend_from_slice(&LIB.push_d_to_stack);
                // push THIS
                cmd_seq.extend(vec![
                    A("THIS"),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None })
                ]);
                cmd_seq.extend_from_slice(&LIB.push_d_to_stack);
                // push THAT
                cmd_seq.extend(vec![
                    A("THAT"),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None })
                ]);
                cmd_seq.extend_from_slice(&LIB.push_d_to_stack);
                // LCL = SP
                cmd_seq.extend(vec![
                    A("SP"),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None }),
                    A("LCL"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // ARG = SP-index-5
                cmd_seq.extend(vec![
                    A(&self.index),
                    C(CCmd{ dest: Some("D"), comp: "D-A", jump: None }),
                    A("5"),
                    C(CCmd{ dest: Some("D"), comp: "D-A", jump: None }),
                    A("ARG"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // goto function
                cmd_seq.extend(vec![
                    A(&self.func),
                    C(CCmd{ dest: None, comp: "0", jump: Some("JMP") }),
                ]);
                // return address label
                cmd_seq.push(L(&self.return_label));
            },
            Return => {
                // FRAME = LCL (saving FRAME to R13)
                cmd_seq.extend(vec![
                    A("LCL"),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None }),
                ]);
                cmd_seq.extend_from_slice(&LIB.d_to_r13);
                // RET = *(FRAME-5) (saving RET to R14)
                cmd_seq.extend(vec![
                    A("5"),
                    C(CCmd{ dest: Some("A"), comp: "D-A", jump: None }),
                    C(CCmd{ dest: Some("D"), comp: "M", jump: None }),
                ]);
                cmd_seq.extend_from_slice(&LIB.d_to_r14);
                // *ARG = pop()
                cmd_seq.extend_from_slice(&LIB.pop_from_stack_to_d);
                cmd_seq.extend(vec![
                    A("ARG"),
                    C(CCmd{ dest: Some("A"), comp: "M", jump: None }),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // SP = ARG + 1
                cmd_seq.extend(vec![
                    A("ARG"),
                    C(CCmd{ dest: Some("D"), comp: "M+1", jump: None }),
                    A("SP"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // THAT = *(FRAME-1)
                cmd_seq.extend_from_slice(&LIB.pop_from_r13_to_d);
                cmd_seq.extend(vec![
                    A("THAT"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // THIS = *(FRAME-2)
                cmd_seq.extend_from_slice(&LIB.pop_from_r13_to_d);
                cmd_seq.extend(vec![
                    A("THIS"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // ARG = *(FRAME-3)
                cmd_seq.extend_from_slice(&LIB.pop_from_r13_to_d);
                cmd_seq.extend(vec![
                    A("ARG"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // LCL = *(FRAME-4)
                cmd_seq.extend_from_slice(&LIB.pop_from_r13_to_d);
                cmd_seq.extend(vec![
                    A("LCL"),
                    C(CCmd{ dest: Some("M"), comp: "D", jump: None }),
                ]);
                // goto RET
                cmd_seq.extend(vec![
                    A("R14"),
                    C(CCmd{ dest: Some("A"), comp: "M", jump: None }),
                    C(CCmd{ dest: None, comp: "0", jump: Some("JMP") }),
                ]);
            },
        }

        cmd_seq
    }
}

// Assembly commands for translations

/// Fields of an assembly C-command
#[derive(Clone)]
pub struct CCmd<'a> {
    pub dest: Option<&'a str>,
    pub comp: &'a str,
    pub jump: Option<&'a str>,
}

/// Possible assembly instruction types
#[derive(Clone)]
pub enum CmdAsm<'a> {
    A(&'a str),
    C(CCmd<'a>),
    L(&'a str),
}

impl<'a> fmt::Display for CmdAsm<'a> {
    /// How to display `CmdAsm`.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CmdAsm::A(ref symbol) => write!(f, "@{}", symbol),
            CmdAsm::L(ref symbol) => write!(f, "({})", symbol),
            CmdAsm::C(ref c_cmd) => {
                let mut cmd_str = String::new();

                if let Some(ref s) = c_cmd.dest {
                    cmd_str.push_str(s);
                    cmd_str.push('=');
                }

                cmd_str.push_str(&c_cmd.comp);

                if let Some(ref s) = c_cmd.jump {
                    cmd_str.push(';');
                    cmd_str.push_str(s);
                }

                write!(f, "{}", cmd_str.as_str())
            }
        }
    }
}

// Check a validity of a VM symbol
fn check_symbol(sym: &str) -> Result<String, String> {
    let char_is_valid = |c: char| {
        let allowed_chars = vec![':', '_', '.'];
        c.is_alphabetic() || c.is_numeric() || allowed_chars.contains(&c)
    };

    let mut err = String::new();
    if sym.is_empty() {
        err = String::from("Symbol is empty.");
    }

    let mut sym_iter = sym.chars().enumerate();
    while err.is_empty() {
        match sym_iter.next() {
            Some((i, c)) => {
                if i==0 && c.is_numeric() {
                    err = format!("Symbol `{}` starts with a digit.",
                                  sym);
                } else if !char_is_valid(c) {
                    err = format!("Char `{}` in symbol `{}` is invalid.",
                                  c, sym);
                }
            },
            None => break,
        }
    }

    match err.is_empty() {
        true => Ok(sym.into()),
        false => Err(err)
    }
}

#[cfg(test)]
mod tests {
    use super::{CmdMem, CmdArith, CmdFlow, CmdFunc,
                CmdAsm, CCmd, check_symbol};

    #[test]
    fn asm_commands_to_string() {
        // A-command
        let cmd = CmdAsm::A("bagr");
        assert_eq!("@bagr", cmd.to_string());
        // L-command
        let cmd = CmdAsm::L("LOOP");
        assert_eq!("(LOOP)", cmd.to_string());
        // C-commands
        // All fields present
        let cmd = CmdAsm::C(CCmd{ dest: Some("M"),
                                    comp: "D+A",
                                    jump: Some("JEQ") });
        assert_eq!("M=D+A;JEQ", cmd.to_string());
        // Only dest + comp
        let cmd = CmdAsm::C(CCmd{ dest: Some("D"),
                                    comp: "M+1",
                                    jump: None });
        assert_eq!("D=M+1", cmd.to_string());
        // Only comp + jump
        let cmd = CmdAsm::C(CCmd{ dest: None,
                                    comp: "0",
                                    jump: Some("JMP") });
        assert_eq!("0;JMP", cmd.to_string());
        // Though technically possible, both dest and jump should not be None
        let cmd = CmdAsm::C(CCmd{ dest: None,
                                    comp: "D",
                                    jump: None });
        assert_eq!("D", cmd.to_string());
    }

    #[test]
    fn func_return() {
        let mut cmd_func = CmdFunc::from("return", "", "")
            .expect("Error creating command.");
        let cmd_func = cmd_func.translate("0");

        let expected = vec![
            "@LCL", "D=M", "@R13", "M=D",
            "@5", "A=D-A", "D=M", "@R14", "M=D",
            "@SP", "AM=M-1", "D=M", "@ARG", "A=M", "M=D",
            "@ARG", "D=M+1", "@SP", "M=D",
            "@R13", "AM=M-1", "D=M", "@THAT", "M=D",
            "@R13", "AM=M-1", "D=M", "@THIS", "M=D",
            "@R13", "AM=M-1", "D=M", "@ARG", "M=D",
            "@R13", "AM=M-1", "D=M", "@LCL", "M=D",
            "@R14", "A=M", "0;JMP",
        ];
        assert_eq!(cmd_func.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_func.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn func_call() {
        let mut cmd_func = CmdFunc::from("call", "bagrfunc", "3")
            .expect("Error creating command.");
        let cmd_func = cmd_func.translate("0");

        let expected = vec![
            "@ret_addr_call:0", "D=A", "@SP", "AM=M+1", "A=A-1", "M=D",
            "@LCL", "D=M", "@SP", "AM=M+1", "A=A-1", "M=D",
            "@ARG", "D=M", "@SP", "AM=M+1", "A=A-1", "M=D",
            "@THIS", "D=M", "@SP", "AM=M+1", "A=A-1", "M=D",
            "@THAT", "D=M", "@SP", "AM=M+1", "A=A-1", "M=D",
            "@SP", "D=M", "@LCL", "M=D",
            "@3", "D=D-A", "@5", "D=D-A", "@ARG", "M=D",
            "@bagrfunc", "0;JMP",
            "(ret_addr_call:0)",
        ];
        assert_eq!(cmd_func.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_func.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn func_function_3() {
        let mut cmd_func = CmdFunc::from("function", "bagrfunc", "3")
            .expect("Error creating command.");
        let cmd_func = cmd_func.translate("");

        let expected = vec!["(bagrfunc)", "@3", "D=A", "(bagrfunc_push_local)",
                            "@SP", "AM=M+1", "A=A-1", "M=0",
                            "@bagrfunc_push_local", "D=D-1;JGT"];
        assert_eq!(cmd_func.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_func.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn func_function_0() {
        let mut cmd_func = CmdFunc::from("function", "bagrfunc", "0")
            .expect("Error creating command.");
        let cmd_func = cmd_func.translate("");

        let expected = vec!["(bagrfunc)"];
        assert_eq!(cmd_func.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_func.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn flow_label() {
        let mut cmd_flow = CmdFlow::from("label", "bagr")
            .expect("Error creating command.");
        let cmd_flow = cmd_flow.translate("func");

        let expected = vec!["(func$bagr)"];
        assert_eq!(cmd_flow.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_flow.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn flow_goto() {
        let mut cmd_flow = CmdFlow::from("goto", "bagr")
            .expect("Error creating command.");
        let cmd_flow = cmd_flow.translate("func");

        let expected = vec!["@func$bagr", "0;JMP"];
        assert_eq!(cmd_flow.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_flow.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn flow_if_goto() {
        let mut cmd_flow = CmdFlow::from("if-goto", "bagr")
            .expect("Error creating command.");
        let cmd_flow = cmd_flow.translate("f");

        let expected = vec!["@SP", "M=M-1", "A=M", "D=M", "@f$bagr", "D;JNE"];
        assert_eq!(cmd_flow.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_flow.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn arith_neg() {
        let mut cmd_mem = CmdArith::from("neg")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@SP", "M=M-1", "A=M", "M=-M", "@SP", "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn arith_sub() {
        let mut cmd_mem = CmdArith::from("sub")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@SP", "M=M-1", "A=M", "D=M", "@SP", "M=M-1",
                            "A=M", "M=M-D", "@SP", "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn arith_eq() {
        let mut cmd_mem = CmdArith::from("eq")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("0");

        let expected = vec!["@SP", "M=M-1", "A=M", "D=M", "@SP", "M=M-1",
                            "A=M", "D=M-D", "@SP", "A=M", "M=-1", "@EQ_END.0",
                            "D;JEQ", "@SP", "A=M", "M=0", "(EQ_END.0)", "@SP",
                            "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn push_constant() {
        let mut cmd_mem = CmdMem::from("push", "constant", "5")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@5", "D=A", "@SP", "A=M", "M=D", "@SP", "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn push_local() {
        let mut cmd_mem = CmdMem::from("push", "local", "1")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@LCL", "D=M", "@1", "A=D+A", "D=M", "@SP", "A=M",
                            "M=D", "@SP", "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn push_temp() {
        let mut cmd_mem = CmdMem::from("push", "temp", "3")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@5", "D=A", "@3", "A=D+A", "D=M", "@SP", "A=M",
                            "M=D", "@SP", "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn push_static() {
        let mut cmd_mem = CmdMem::from("push", "static", "3")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("bagr");

        let expected = vec!["@bagr.3", "D=M", "@SP", "A=M", "M=D", "@SP",
                            "M=M+1"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    #[should_panic(expected = "Invalid index=2 for `pointer` segment. Maximum \
                               is 1.")]
    fn err_pointer_segment_idx_range() {
        CmdMem::from("push", "pointer", "2").unwrap();
    }

    #[test]
    #[should_panic(expected = "Invalid index=8 for `temp` segment. Maximum \
                               is 7.")]
    fn err_temp_segment_idx_range() {
        CmdMem::from("pop", "temp", "8").unwrap();
    }

    #[test]
    #[should_panic(expected = "`pop constant` is not allowed.")]
    fn err_pop_constant() {
        CmdMem::from("pop", "constant", "5").unwrap();
    }

    #[test]
    fn pop_local() {
        let mut cmd_mem = CmdMem::from("pop", "local", "2")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@LCL", "D=M", "@2", "D=D+A", "@R13", "M=D",
                            "@SP", "M=M-1", "A=M", "D=M", "@R13", "A=M",
                            "M=D"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate() {
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn pop_temp() {
        let mut cmd_mem = CmdMem::from("pop", "temp", "3")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("");

        let expected = vec!["@5", "D=A", "@3", "D=D+A", "@R13", "M=D", "@SP",
                            "M=M-1", "A=M", "D=M", "@R13", "A=M", "M=D"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn pop_static() {
        let mut cmd_mem = CmdMem::from("pop", "static", "3")
            .expect("Error creating command.");
        let cmd_mem = cmd_mem.translate("bagr");

        let expected = vec!["@bagr.3", "D=A", "@R13", "M=D", "@SP", "M=M-1",
                            "A=M", "D=M", "@R13", "A=M", "M=D"];
        assert_eq!(cmd_mem.len(), expected.len(),
                   "Wrong number of translated commands.");

        let it = cmd_mem.iter().zip(expected.into_iter());
        for (i, (cmd, exp)) in it.enumerate(){
            assert_eq!(cmd.to_string(), exp,
                       "Error comparing {}th command.", i);
        }
    }

    #[test]
    fn check_symbol_valid() {
        let inp = vec!["ax1AX", "unicode:öÄýRž", "special:._"];
        for s in inp {
            assert_eq!(check_symbol(s), Ok(String::from(s)));
        }
    }

    #[test]
    #[should_panic(expected = "Symbol is empty.")]
    fn err_check_symbol_empty() {
        let inp = "";
        check_symbol(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Symbol `0xx` starts with a digit.")]
    fn err_check_symbol_digit() {
        let inp = "0xx";
        check_symbol(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Char `-` in symbol `-125` is invalid.")]
    fn err_check_symbol_invalid_char_1() {
        let inp = "-125";
        check_symbol(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Char `+` in symbol `abc+x` is invalid.")]
    fn err_check_symbol_invalid_char_2() {
        let inp = "abc+x";
        check_symbol(inp).unwrap();
    }
}
