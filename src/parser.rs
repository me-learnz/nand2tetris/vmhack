//! `Parser` reads an opened input file, detects commands and matches them to
//! their types with proper arguments.
//!
//! The data type used for storing the instruction information is [`CmdVM`].
//!
//! `Parser` checks whether the instruction fits the VM format. Validity of
//! used keywords is also checked. All output coming from `Parser` can be
//! considered as valid.
//!
//! [`CmdVM`]: ../cmd_types/enum.CmdVM.html

use std::io::{BufRead, BufReader};
use std::fs::File;

use cmd_types::{CmdArith, CmdMem, CmdFlow, CmdFunc, CmdVM};

/// The `Parser` struct used for parsing input Hack assembly file.
///
/// The `Parser` operates on an opened file. It reads the file line by line,
/// detecting whether the line is a command, which type of command and what
/// arguments have been used.
pub struct Parser {
    /// Current parsed command is stored in `cmd`.
    ///
    /// The value is initialized to [`None`] when `Parser` is created.
    ///
    /// [`None`]: https://doc.rust-lang.org/stable/std/option/enum.Option.html#variant.None
    pub cmd: Option<CmdVM>,
    next_cmd: Option<String>,
    reader: BufReader<File>,
}

impl Parser {
    /// Create a new instance of `Parser`.
    ///
    /// Needs a file handle of an opened file. Then, it creates a new
    /// [`BufReader`] which is used for reading the file.
    ///
    /// The method also performs a search for the next command.
    ///
    /// [`BufReader`]: https://doc.rust-lang.org/stable/std/io/struct.BufReader.html
    pub fn new(fh: File) -> Parser {
        let mut parser = Parser {
            cmd: None,
            next_cmd: None,
            reader: BufReader::new(fh),
        };
        parser.next_command();
        parser
    }

    /// Checks whether there are more commands in the VM file.
    ///
    /// This function returns `true` if there is at least one command in the
    /// assembly file which has not been read yet.
    pub fn has_more_commands(&self) -> bool {
        self.next_cmd.is_some()
    }

    /// Save the next available command in the [`cmd`] field of `Parser`.
    ///
    /// First, the method finds a next command, skipping empty lines or lines
    /// with only comments. If a command was found, it gets decoded and stored
    /// in `Parser's` [`cmd'] field. Otherwise, nothing happens.
    ///
    /// # Panics
    ///
    /// Panics when a command
    ///
    /// * has syntax errors
    /// * uses unrecognized command
    /// * uses unrecognized arguments
    ///
    /// [`cmd`]: #structfield.cmd
    /// [`CmdVM`]: ../cmd_types/enum.CmdVM.html
    /// [`has_more_commands`]: #method.has_more_commands
    pub fn advance(&mut self) {
        if self.has_more_commands() {
            let cmd_line = self.next_cmd.take()
                .expect("No next command saved in Parser");
            self.cmd = Some(decode_command(&cmd_line)
                .expect("Error decoding VM command"));
        }
        self.next_command();
    }

    fn next_line_trimmed(&mut self) -> LineType {
        let mut line = String::new();
        self.reader.read_line(&mut line).expect("Error reading line.");

        if line.is_empty() {
            LineType::EOF
        } else {
            line = trim_line(&line);
            if line.is_empty() {
                LineType::Empty
            } else {
                LineType::Line(line)
            }
        }
    }

    fn next_command(&mut self) {
        while self.next_cmd.is_none() {
            match self.next_line_trimmed() {
                LineType::EOF => {
                    self.next_cmd = None;
                    break;
                },
                LineType::Empty => (),
                LineType::Line(cmd_line) => {
                    self.next_cmd = Some(cmd_line);
                    break;
                },
            }
        }
    }
}

// Enums

// Used in command decoding
enum LineType {
    EOF,
    Empty,
    Line(String),
}

// Helper functions

/// Remove comments and all surrounding whitespace from a given line.
fn trim_line(line: &str) -> String {
    match line.find("//") {
        Some(pos) => line[..pos].trim().into(),
        None => line.trim().into(),
    }
}

/// Decode a VM command into corresponding parts.
/// Assumes that input has been cleaned with `trim_line`.
fn decode_command(s: &str) -> Result<CmdVM, String> {
    let words: Vec<&str> = s.split_whitespace().collect();

    match words.len() {
        // Arithmetic command or `return` Function command
        1 => {
            if s == "return" {
                let func_command = CmdFunc::from(s, "", "")?;
                Ok(CmdVM::Func(func_command))
            } else {
                let arith_command = CmdArith::from(s)?;
                Ok(CmdVM::Arith(arith_command))
            }
        },
        // Program flow command
        2 => {
            let (cmd, arg1) = (words[0], words[1]);
            let flow_command = CmdFlow::from(cmd, arg1)?;
            Ok(CmdVM::Flow(flow_command))
        },
        // Memory access or Function command
        3 => {
            let (cmd, arg1, arg2) = (words[0], words[1], words[2]);
            if cmd == "push" || cmd == "pop" {
                let mem_command = CmdMem::from(cmd, arg1, arg2)?;
                Ok(CmdVM::Mem(mem_command))
            } else {
                let func_command = CmdFunc::from(cmd, arg1, arg2)?;
                Ok(CmdVM::Func(func_command))
            }
        },
        _ => Err(format!("Invalid number of arguments in command `{}`.", s)),
    }
}

#[cfg(test)]
mod tests {
    use cmd_types::CmdVM;

    use super::{decode_command};

    #[test]
    fn decode_arith() {
        // Allowed
        let inp_vec = vec!["add", "sub", "neg", "eq", "gt", "lt", "and", "or",
                           "not"];
        for inp in inp_vec.iter() {
            match decode_command(inp) {
                Ok(CmdVM::Arith(_)) => (),
                Ok(c) => panic!("Decoding `{}` resulted in {:?}, not a \
                                `CmdArith` type.", inp, c),
                Err(e) => panic!("Error decoding `{}`: {}", inp, e),
            }
        }
        let inp = "add";
        if let Ok(CmdVM::Arith(_)) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` did not return `CmdArith` type.",
                   inp);
        }

        let inp = "Add";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }

        let inp = "bagr";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }
    }

    #[test]
    fn decode_mem() {
        // Allowed
        let inp_vec = vec!["push constant 5", "pop static 13"];
        for inp in inp_vec.iter() {
            match decode_command(inp) {
                Ok(CmdVM::Mem(_)) => (),
                Ok(c) => panic!("Decoding `{}` resulted in {:?}, not a \
                                `CmdMem` type.", inp, c),
                Err(e) => panic!("Error decoding `{}`: {}", inp, e),
            }
        }

        // `pop constant` should be disallowed
        let inp = "pop constant 5";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }

        let inp = "Pop static 0";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }

        let inp = "push bagr 13";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }

        let inp = "push temp -1";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }
    }

    #[test]
    fn decode_flow() {
        // Allowed
        let inp_vec = vec!["label bagr", "goto ba_r", "if-goto .4:r"];
        for inp in inp_vec.iter() {
            match decode_command(inp) {
                Ok(CmdVM::Flow(_)) => (),
                Ok(c) => panic!("Decoding `{}` resulted in {:?}, not a \
                                `CmdFlow` type.", inp, c),
                Err(e) => panic!("Error decoding `{}`: {}", inp, e),
            }
        }

        // `$` should not be allowed
        let inp = "if-goto ba$gr";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }

        let inp = "Label bagr";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }
    }

    #[test]
    fn decode_func() {
        // Allowed
        let inp_vec = vec!["return", "call Main.main 0", "function bagr 2"];
        for inp in inp_vec.iter() {
            match decode_command(inp) {
                Ok(CmdVM::Func(_)) => (),
                Ok(c) => panic!("Decoding `{}` resulted in {:?}, not a \
                                `CmdFunc` type.", inp, c),
                Err(e) => panic!("Error decoding `{}`: {}", inp, e),
            }
        }

        let inp = "return extra args";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }

        let inp = "call invalid_func$name 0";
        if let Err(_) = decode_command(inp) {
        } else {
            panic!("Decoding `{}` was successful but it shouldn't be.", inp);
        }
    }

    #[test]
    #[should_panic(expected = "Invalid number of arguments in command \
                               `push this 2 extra`.")]
    fn decode_err_num_args() {
        let inp = "push this 2 extra";
        decode_command(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Invalid number of arguments in command `  ")]
    fn decode_err_num_args_empty() {
        let inp = "  \t  \n \t  ";
        decode_command(inp).unwrap();
    }
}
