extern crate clap;
#[cfg(test)]
extern crate tempdir;

use std::path::PathBuf;
use std::fs::{self, File, remove_file};

use clap::{Arg, App};

use parser::Parser;
use code_writer::CodeWriter;

pub mod parser;
pub mod cmd_types;
pub mod code_writer;

fn main() {
    // CLI
    let matches = App::new("vmhack")
                          .version("0.0.0")
                          .author("Jakub Žádník <kubouch@gmail.com>")
                          .about("Translator of Hack VM language into Hack \
                                  assembly language from www.nand2tetris.org.")
                          .arg(Arg::with_name("INPUT")
                               .help("Input VM file or a directory.")
                               .required(true)
                               .index(1))
                          .arg(Arg::with_name("output")
                               .short("o")
                               .long("output")
                               .value_name("FILE")
                               .help("Optional output file.")
                               .takes_value(true))
                          .arg(Arg::with_name("verbose")
                               .short("v")
                               .long("verbose")
                               .help("Print info, otherwise be silent. \
                                      Errors are still shown."))
                          .arg(Arg::with_name("no-init")
                               .long("no-init")
                               .help("Do not write init code."))
                          .get_matches();

    // CLI args

    // verbose flag
    let verbose = matches.is_present("verbose");

    // do-not-write-bootstrap-code flag
    let no_init = matches.is_present("no-init");

    // Detect input VM files
    let inp_path = PathBuf::from(matches.value_of("INPUT")
        .expect("Error crating input path"));
    let inp_path = fs::canonicalize(inp_path)
        .expect("Error creating absolute path from input path");

    let mut vm_files: Vec<PathBuf> = Vec::new();

    match inp_path.is_dir() {
        true => {
            let files = fs::read_dir(&inp_path)
                .expect("Error reading files in input directory");
            for file in files {
                let file = file.expect("No files.").path();
                if file.extension().expect("Extension error.") == "vm" {
                    if verbose {
                        println!("Input VM file: {}", file.display());
                    }
                    vm_files.push(file);
                }
            }
            // Error if no files were found in directory
            if vm_files.is_empty() {
                panic!("Directory {} does not contain any files with .vm \
                        extension!", inp_path.display());
            }
        },
        false => {
            vm_files.push(inp_path.to_path_buf());
            println!("Input VM file: {}", inp_path.display());
        },
    }

    // Setup output file path
    let out_file = match matches.value_of("output") {
        None => {
            let out_path = match inp_path.is_dir() {
                true => inp_path.join(inp_path.file_name()
                    .expect("Wrong filename")),
                false => PathBuf::from(&inp_path),
            };
            out_path.with_extension("asm")
        },
        Some(o_arg) => PathBuf::from(o_arg),
    };
    if verbose {
        println!("Output saved to: {}", out_file.display());
    }

    // Create a code writer
    let mut w = CodeWriter::new(&out_file)
                  .expect("Error creating CodeWriter");
    if !no_init {
        w.write_init();
    }

    let mut commands_were_found = false;

    // Loop over input VM files
    for vm_file in vm_files {
        // Open input file
        let fh_in = File::open(&vm_file);
        let fh_in = match fh_in {
            Ok(fh) => fh,
            Err(e) => panic!("Error opening {}:\n{}\n", vm_file.display(), e)
        };

        // Create a parser
        let mut p: Parser = Parser::new(fh_in);  // takes ownership of fh_in

        w.set_file(&vm_file);

        // Write commands to asm file
        while p.has_more_commands() {
            if !commands_were_found {
                commands_were_found = true;
            }

            p.advance();
            match p.cmd {
                Some(ref mut cmd) =>  w.write_cmd(cmd),
                None => panic!("No command!"),
            }
        }
    }

    // Remove output file in case of no VM commands
    if !commands_were_found {
        drop(w);
        remove_file(&out_file).expect(&format!("Error removing {}",
                                      out_file.display()));
        panic!(format!("No VM instructions found in `{}`. No output was \
                        written", inp_path.display()));
    }
}
